# Python-PONG
A simple PONG-like game written in Python3 using Pygame.

## Table of contents
* [Description](#description)
* [Dependencies](#dependencies)
* [Setup](#setup)
	
## Description
The aim of this game is to bounce the ball so that it passes through area behind the opponent.
For single-player mode use up/down arrows.
For multi-players mode use also 'w'/'s' keys.

## Dependencies
Program requires following libraries to be installed:

* pygame
	
## Setup
Download Pygame:
```
$ pip3 install pygame
```

Run the program:
```
$ python3 PONG.py
```
