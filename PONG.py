"""PONG game
Created by Maciej Stępień"""

import pygame
import sys
import random
import math


def loadHighscores():
    """Loads highscores from a file if file
    exists, otherwise creates new dictionary
    containing highcores set to 0."""
    global highscores
    try:
        file = open("highscores.txt")
        highscores = {
            "HARD": [0, 0],
            "MEDIUM": [0, 0],
            "EASY": [0, 0],
            "MULTIPLAYER": [0, 0]
        }
        words = file.readline().split()
        highscores["HARD"] = [int(words[0]), int(words[1])]

        words = file.readline().strip().split()
        highscores["MEDIUM"] = [int(words[0]), int(words[1])]

        words = file.readline().strip().split()
        highscores["EASY"] = [int(words[0]), int(words[1])]

        words = file.readline().strip().split()
        highscores["MULTIPLAYER"] = [int(words[0]), int(words[1])]

        file.close()
    except IOError:
        highscores = {
            "HARD": [0, 0],
            "MEDIUM": [0, 0],
            "EASY": [0, 0],
            "MULTIPLAYER": [0, 0]
        }


def saveHighscores():
    """Saves highscores to file."""
    global highscores
    file = open('highscores.txt', 'w')
    file.write(
        "{} {}\n".format(
            highscores.get("HARD")[0],
            highscores.get("HARD")[1]))
    file.write(
        "{} {}\n".format(
            highscores.get("MEDIUM")[0],
            highscores.get("MEDIUM")[1]))
    file.write(
        "{} {}\n".format(
            highscores.get("EASY")[0],
            highscores.get("EASY")[1]))
    file.write(
        "{} {}".format(
            highscores.get("MULTIPLAYER")[0],
            highscores.get("MULTIPLAYER")[1]))
    file.close()


def createButton(text, size):
    """Return created Button - dictionary containing
    white and black text and also bounds."""
    font = pygame.font.Font('freesansbold.ttf', size)
    blackText = font.render(str(text), True, black)
    whiteText = font.render(str(text), True, white)
    textBox = whiteText.get_rect()
    button = {
        "rect": textBox,
        "whiteText": whiteText,
        "blackText": blackText
    }
    return button


def drawButton(button):
    """Draws Button, and checks if mouse
    is inside bounds of Button."""
    global white, black
    border = 2
    rect = button.get("rect")
    if button.get("rect").collidepoint(pygame.mouse.get_pos()):
        pygame.draw.rect(scr, white, rect)
        scr.blit(button.get("blackText"), rect)
    else:
        pygame.draw.rect(
            scr,
            white,
            (rect.left -
             border,
             rect.top -
             border,
             rect.width +
             border *
             2,
             rect.height +
             border *
             2))
        pygame.draw.rect(scr, black, rect)
        scr.blit(button.get("whiteText"), rect)


def isButtonPressed(button):
    """Checks if Button is pressed and
    returns True or False."""
    global events
    click = pygame.mouse.get_pressed()
    if pygame.MOUSEBUTTONUP in events and button.get(
            "rect").collidepoint(pygame.mouse.get_pos()):
        return True
    return False


def handleInput():
    """Adds detected events to the global
    list of events. Method should be called
    in every tick."""
    global events
    events = []
    for event in pygame.event.get():
        if event.type == pygame.MOUSEBUTTONUP:
            events.append(pygame.MOUSEBUTTONUP)
        if event.type == pygame.QUIT:
            sys.exit()
    keys = pygame.key.get_pressed()

    if keys[pygame.K_s]:
        events.append(pygame.K_s)
    if keys[pygame.K_w]:
        events.append(pygame.K_w)
    if keys[pygame.K_UP]:
        events.append(pygame.K_UP)
    if keys[pygame.K_DOWN]:
        events.append(pygame.K_DOWN)


def showScoreboard():
    global scr, clock, white, black, highscores

    container = pygame.Rect(
        0,
        0,
        round(
            scr.get_rect().width *
            0.8),
        round(
            scr.get_rect().height *
            0.8))
    container.center = scr.get_rect().center

    fontSize = round(scr.get_rect().height * 0.06)
    font = pygame.font.Font('freesansbold.ttf', fontSize)

    # rendering fonts
    first = font.render("1st", True, white)
    second = font.render("2nd", True, white)
    player = font.render("PLAYER", True, white)
    human = font.render("HUMAN", True, white)
    computer = font.render("COMPUTER", True, white)
    hard = font.render("HARD", True, white)
    medium = font.render("MEDIUM", True, white)
    easy = font.render("EASY", True, white)
    multiplayer = font.render("MULTIPLAYER", True, white)

    firstScore = font.render(
        str(highscores.get("MULTIPLAYER")[0]), True, white)
    secondScore = font.render(
        str(highscores.get("MULTIPLAYER")[1]), True, white)
    hardComputerScore = font.render(
        str(highscores.get("HARD")[0]), True, white)
    hardHumanScore = font.render(str(highscores.get("HARD")[1]), True, white)
    mediumComputerScore = font.render(
        str(highscores.get("MEDIUM")[0]), True, white)
    mediumHumanScore = font.render(
        str(highscores.get("MEDIUM")[1]), True, white)
    easyComputerScore = font.render(
        str(highscores.get("EASY")[0]), True, white)
    easyHumanScore = font.render(str(highscores.get("EASY")[1]), True, white)

    # preparing text boxes
    firstBox = first.get_rect()
    secondBox = second.get_rect()
    playerBox = player.get_rect()
    humanBox = human.get_rect()
    computerBox = computer.get_rect()
    hardBox = hard.get_rect()
    mediumBox = medium.get_rect()
    easyBox = easy.get_rect()
    multiplayerBox = multiplayer.get_rect()

    firstScoreBox = firstScore.get_rect()
    secondScoreBox = secondScore.get_rect()
    hardComputerScoreBox = hardComputerScore.get_rect()
    hardHumanScoreBox = hardHumanScore.get_rect()
    mediumComputerScoreBox = mediumComputerScore.get_rect()
    mediumHumanScoreBox = mediumHumanScore.get_rect()
    easyComputerScoreBox = easyComputerScore.get_rect()
    easyHumanScoreBox = easyHumanScore.get_rect()

    # positioning text boxes
    multiplayerBox.left = container.left
    multiplayerBox.top = container.top + firstBox.height
    hardBox.left = container.left
    hardBox.top = multiplayerBox.bottom + computerBox.height
    mediumBox.left = container.left
    mediumBox.top = hardBox.bottom
    easyBox.left = container.left
    easyBox.top = mediumBox.bottom

    firstScoreBox.left = multiplayerBox.right + fontSize
    firstScoreBox.bottom = multiplayerBox.bottom
    secondScoreBox.left = firstScoreBox.right + fontSize * 2
    secondScoreBox.bottom = firstScoreBox.bottom

    firstBox.midbottom = firstScoreBox.midtop
    secondBox.midbottom = secondScoreBox.midtop
    playerBox.left = secondBox.right + fontSize
    playerBox.top = container.top

    computerBox.left = firstBox.left
    computerBox.top = firstScoreBox.bottom
    humanBox.left = computerBox.right + fontSize
    humanBox.top = secondScoreBox.bottom

    hardComputerScoreBox.midtop = computerBox.midbottom
    mediumComputerScoreBox.midtop = hardComputerScoreBox.midbottom
    easyComputerScoreBox.midtop = mediumComputerScoreBox.midbottom

    hardHumanScoreBox.midtop = humanBox.midbottom
    mediumHumanScoreBox.midtop = hardHumanScoreBox.midbottom
    easyHumanScoreBox.midtop = mediumHumanScoreBox.midbottom

    backButton = createButton("BACK", round(scr.get_rect().height * 0.1))
    backButton.get("rect").left = scr.get_rect().left
    backButton.get("rect").bottom = scr.get_rect().bottom
    while True:
        handleInput()

        if isButtonPressed(backButton):
            break

        scr.fill(black)
        drawButton(backButton)
        scr.blit(first, firstBox)
        scr.blit(second, secondBox)
        scr.blit(player, playerBox)
        scr.blit(human, humanBox)
        scr.blit(computer, computerBox)
        scr.blit(hard, hardBox)
        scr.blit(medium, mediumBox)
        scr.blit(easy, easyBox)
        scr.blit(multiplayer, multiplayerBox)

        scr.blit(firstScore, firstScoreBox)
        scr.blit(secondScore, secondScoreBox)
        scr.blit(hardComputerScore, hardComputerScoreBox)
        scr.blit(hardHumanScore, hardHumanScoreBox)
        scr.blit(mediumComputerScore, mediumComputerScoreBox)
        scr.blit(mediumHumanScore, mediumHumanScoreBox)
        scr.blit(easyComputerScore, easyComputerScoreBox)
        scr.blit(easyHumanScore, easyHumanScoreBox)
        pygame.display.flip()
        clock.tick(60)


def showMenu():
    global scr, clock, white, black
    playSingleButton = createButton(
        "PLAY SINGLE", round(
            scr.get_rect().height * 0.1))
    playMultiButton = createButton(
        "PLAY MULTI", round(
            scr.get_rect().height * 0.1))
    scoreboardButton = createButton(
        "LEADERBOARD", round(
            scr.get_rect().height * 0.1))
    exitButton = createButton("EXIT", round(scr.get_rect().height * 0.1))

    playSingleButton.get("rect").center = scr.get_rect().center
    playMultiButton.get("rect").center = scr.get_rect().center
    scoreboardButton.get("rect").center = scr.get_rect().center
    exitButton.get("rect").center = scr.get_rect().center

    playSingleButton.get("rect").top = round(scr.get_rect().height * 0.1)
    playMultiButton.get("rect").top = round(scr.get_rect().height * 0.4)
    scoreboardButton.get("rect").top = round(scr.get_rect().height * 0.7)
    exitButton.get("rect").top = round(scr.get_rect().height * 0.9)
    while True:
        handleInput()

        if isButtonPressed(playMultiButton):
            showPlayScreen("MULTIPLAYER")
            break
        elif isButtonPressed(playSingleButton):
            showLevelScreen()
            break
        elif isButtonPressed(scoreboardButton):
            showScoreboard()
        elif isButtonPressed(exitButton):
            sys.exit()

        scr.fill(black)
        drawButton(playSingleButton)
        drawButton(playMultiButton)
        drawButton(scoreboardButton)
        drawButton(exitButton)

        pygame.display.flip()
        clock.tick(60)


def showLevelScreen():
    global scr, clock, white, black, modeType
    playEasyButton = createButton("EASY", round(scr.get_rect().height * 0.1))
    playMediumButton = createButton(
        "MEDIUM", round(
            scr.get_rect().height * 0.1))
    playHardButton = createButton("HARD", round(scr.get_rect().height * 0.1))
    backButton = createButton("BACK", round(scr.get_rect().height * 0.1))

    playEasyButton.get("rect").center = scr.get_rect().center
    playMediumButton.get("rect").center = scr.get_rect().center
    playHardButton.get("rect").center = scr.get_rect().center
    backButton.get("rect").left = scr.get_rect().left

    playEasyButton.get("rect").top = round(scr.get_rect().height * 0.1)
    playMediumButton.get("rect").top = round(scr.get_rect().height * 0.4)
    playHardButton.get("rect").top = round(scr.get_rect().height * 0.7)
    backButton.get("rect").bottom = round(scr.get_rect().bottom)

    clock.tick(60)
    while True:
        handleInput()

        if isButtonPressed(playEasyButton):
            showPlayScreen("EASY")
            break
        if isButtonPressed(playMediumButton):
            showPlayScreen("MEDIUM")
            break
        if isButtonPressed(playHardButton):
            showPlayScreen("HARD")
            break
        if isButtonPressed(backButton):
            showMenu()
            break

        scr.fill(black)
        drawButton(playEasyButton)
        drawButton(playMediumButton)
        drawButton(playHardButton)
        drawButton(backButton)

        pygame.display.flip()
        clock.tick(60)


def showPauseScreen():
    global scr, clock, white, black, modeType
    continueButton = createButton(
        "CONTINUE", round(
            scr.get_rect().height * 0.1))
    menuButton = createButton("MENU", round(scr.get_rect().height * 0.1))
    exitButton = createButton("EXIT", round(scr.get_rect().height * 0.1))

    continueButton.get("rect").center = scr.get_rect().center
    menuButton.get("rect").center = scr.get_rect().center
    exitButton.get("rect").center = scr.get_rect().center

    continueButton.get("rect").top = round(scr.get_rect().height * 0.1)
    menuButton.get("rect").top = round(scr.get_rect().height * 0.4)
    exitButton.get("rect").top = round(scr.get_rect().height * 0.7)

    while True:
        handleInput()

        if isButtonPressed(continueButton):
            break
        if isButtonPressed(menuButton):
            showMenu()
            break
        if isButtonPressed(exitButton):
            sys.exit()
            break

        scr.fill(black)
        drawButton(continueButton)
        drawButton(menuButton)
        drawButton(exitButton)

        pygame.display.flip()
        clock.tick(60)


def showPlayScreen(mode):
    global white, black, scr, clock

    def randColor():
        """Returns random generated color."""
        rgb = random.randint(0, 0xFFFFFFFF)
        color = pygame.color.Color(rgb)
        return color

    def standarizateAngle(angle):
        """Returns converted angle to the range 0 - 359."""
        while angle < 0:
            angle += 360
        angle %= 360
        return angle

    def limitRotation(rotation):
        """Returns limited angle rotation, which absolute
        deviation from horizontal line is less or equal to 60."""
        rotation = standarizateAngle(rotation)
        if rotation > 60 and rotation < 120:
            if rotation < 90:
                rotation = 60
            else:
                rotation = 120
        if rotation < 300 and rotation > 240:
            if rotation < 270:
                rotation = 240
            else:
                rotation = 300
        return rotation

    def generateMovementVector(speed, rotation):
        """Returns movement vector, based on
        speed and rotation of the ball."""
        rotation = standarizateAngle(rotation)
        vector = []
        vector.append(math.cos(rotation * math.pi / 180) * speed)
        vector.append(math.sin(rotation * math.pi / 180) * speed)

        return vector

    def goal(player):
        """Updates score, refreshes scoreboard,
        sets ball to the center of area, when goal gained."""
        global white, black
        nonlocal ball, p1Text, p2Text
        scores[player] += 1
        if player == "player1":
            ball["rotation"] = random.randint(150, 210)
            p1Text = myfont.render(str(scores.get("player1")), True, white)
        elif player == "player2":
            ball["rotation"] = random.randint(-30, 30)
            p2Text = myfont.render(str(scores.get("player2")), True, white)
        ball["speed"] = 300
        ball["position"] = area.center
        updateHighscore()

    def updateHighscore():
        """If score of player1 or player2 is
        higher than highest score, then sets
        new highscore and saves it to file."""
        global highscores
        nonlocal mode
        if max(highscores.get(mode)) < scores.get("player1") or max(
                highscores.get(mode)) < scores.get("player2"):
            highscores[mode] = [scores.get("player1"), scores.get("player2")]
            saveHighscores()

    def drawBorderLine(screen, color, area):
        """Draws vertical line, separating court halfs."""
        rect = pygame.Rect(0, 0, 4, 10)
        rect.center = area.center
        rectNum = int(area.height / rect.height / 2)
        offset = round((rect.height - area.height % rect.height) / 2)
        for n in range(0, rectNum + 1):
            rect.y = area.top + offset + 2 * n * rect.height
            pygame.draw.rect(screen, color, rect)

    def movePlayer(player, step):
        """Moves given player by given step
        if doesn't go outside of game area."""
        nonlocal area
        step = round(step)
        if player.bottom + step > area.bottom:
            player.bottom = area.bottom
        elif player.top + step < area.top:
            player.top = area.top
        else:
            player.bottom += step

    scoreBoard = scr.get_rect()
    scoreBoard.height = round(scoreBoard.height * 0.1)
    area = scr.get_rect()
    area.height = scr.get_rect().height - scoreBoard.height
    area.top = scoreBoard.bottom

    player1 = pygame.Rect(0, 0, 10, int(area.height * 0.2))
    player1.midleft = area.midleft
    player2 = pygame.Rect(0, 0, 10, int(area.height * 0.2))
    player2.midright = area.midright

    pauseButton = createButton("||", round(scoreBoard.height - 8))
    pauseButton.get("rect").center = scoreBoard.center
    pauseButton.get("rect").top = scoreBoard.top

    color = randColor()
    scores = {
        "player1": 0,
        "player2": 0
    }

    myfont = pygame.font.Font('freesansbold.ttf', int(scoreBoard.height * 0.8))
    p1Text = myfont.render(str(scores.get("player1")), True, white)
    p2Text = myfont.render(str(scores.get("player2")), True, white)

    p1TextBox = p1Text.get_rect()
    p1TextBox.center = [round(scoreBoard.width * 0.25), scoreBoard.center[1]]

    p2TextBox = p2Text.get_rect()
    p2TextBox.center = [round(scoreBoard.width * 0.75), scoreBoard.center[1]]

    ball = {
        "speed": 300,
        "rotation": random.randint(-30, 30),
        "color": randColor(),
        "radius": 10,
        "position": area.center
    }

    step = 1
    dt = 0
    while True:
        handleInput()

        # keyboard input to player movement
        tempCoefficient = dt * area.height
        if mode == "MULTIPLAYER":
            if pygame.K_s in events:
                movePlayer(player1, tempCoefficient * step)
            if pygame.K_w in events:
                movePlayer(player1, -tempCoefficient * step)
        elif mode == "EASY":
            delta = ball.get("position")[1] - player1.center[1]
            if abs(delta) >= tempCoefficient * 0.5:
                if delta != 0:
                    delta /= abs(delta)
                movePlayer(player1, tempCoefficient * delta * 0.5)
        elif mode == "MEDIUM":
            delta = ball.get("position")[1] - player1.center[1]
            if abs(delta) >= tempCoefficient:
                if delta != 0:
                    delta /= abs(delta)
                movePlayer(player1, tempCoefficient * delta)
        elif mode == "HARD":
            delta = ball.get("position")[1] - player1.center[1]
            if abs(delta) >= tempCoefficient * 2:
                if delta != 0:
                    delta /= abs(delta)
                movePlayer(player1, tempCoefficient * delta * 2)
        if pygame.K_DOWN in events:
            movePlayer(player2, tempCoefficient * step)
        if pygame.K_UP in events:
            movePlayer(player2, -tempCoefficient * step)

        # ball out of horizontal bounds
        if ball.get("position")[1] - ball.get("radius") < area.top:
            ball["rotation"] = -ball.get("rotation")
            ball["position"][1] = area.top + ball.get("radius")
        elif ball.get("position")[1] + ball.get("radius") > area.bottom:
            ball["rotation"] = -ball.get("rotation")
            ball["position"][1] = area.bottom - ball.get("radius")

        # ball collides with players
        # checks if ball collides with 1st player
        if ball.get("position")[0] - ball.get("radius") < player1.right and \
                ((ball.get("position")[1] + ball.get("radius") > player1.top and
                  ball.get("position")[1] + ball.get("radius") < player1.bottom) or
                 (ball.get("position")[1] - ball.get("radius") > player1.top and
                  ball.get("position")[1] - ball.get("radius") < player1.bottom)):

            # checks if ball collides with top horizontal edge
            if ball.get("position")[1] < player1.top and (
                abs(
                    ball.get("position")[1] +
                    ball.get("radius") -
                    player1.top) < abs(
                    ball.get("position")[0] -
                    ball.get("radius") -
                    player1.right)):
                ball["rotation"] = -ball.get("rotation")
                ball.get("position")[1] = player1.top - ball.get("radius")
            # checks if ball collides with bottom horizontal edge
            elif ball.get("position")[1] > player1.bottom and \
                    (abs(ball.get("position")[1] - ball.get("radius") - player1.bottom) < abs(
                        ball.get("position")[0] - ball.get("radius") - player1.right)):
                ball["rotation"] = -ball.get("rotation")
                ball.get("position")[1] = player1.bottom + ball.get("radius")
            # checks if ball collides with vertical edge
            else:
                ball["rotation"] = 180 - \
                    ball.get("rotation") + random.randint(-30, 30)
                ball["speed"] += 20
                ball.get("position")[0] = player1.right + ball.get("radius")

        # checks if ball collides with 2nd player
        elif ball.get("position")[0] + ball.get("radius") > player2.left and \
                ((ball.get("position")[1] + ball.get("radius") > player2.top and
                  ball.get("position")[1] + ball.get("radius") < player2.bottom) or
                 (ball.get("position")[1] - ball.get("radius") > player2.top and
                  ball.get("position")[1] - ball.get("radius") < player2.bottom)):

            # checks if ball collides with top horizontal edge
            if ball.get("position")[1] < player2.top and (
                abs(
                    ball.get("position")[1] +
                    ball.get("radius") -
                    player2.top) < abs(
                    ball.get("position")[0] +
                    ball.get("radius") -
                    player2.left)):
                ball["rotation"] = -ball.get("rotation")
                ball.get("position")[1] = player2.top - ball.get("radius")
            # checks if ball collides with bottom horizontal edge
            elif ball.get("position")[1] > player2.bottom and \
                    (abs(ball.get("position")[1] - ball.get("radius") - player2.bottom) < abs(
                        ball.get("position")[0] + ball.get("radius") - player2.left)):
                ball["rotation"] = -ball.get("rotation")
                ball.get("position")[1] = player2.bottom + ball.get("radius")
                # checks if ball collides with vertical edge
            else:
                ball["rotation"] = 180 - \
                    ball.get("rotation") + random.randint(-30, 30)
                ball["speed"] += 20
                ball.get("position")[0] = player2.left - ball.get("radius")

        # ball out of vertical bounds - goal
        elif ball.get("position")[0] < area.left:
            ball["rotation"] = 180 - ball.get("rotation")
            goal("player2")
            color = randColor()
        elif ball.get("position")[0] > area.right:
            ball["rotation"] = 180 - ball.get("rotation")
            goal("player1")
            color = randColor()

        if isButtonPressed(pauseButton):
            showPauseScreen()

        ball["rotation"] = limitRotation(ball.get("rotation"))
        vec = generateMovementVector(
            ball.get("speed") * dt,
            ball.get("rotation"))
        ball["position"] = [
            ball.get("position")[0] + vec[0],
            ball.get("position")[1] + vec[1]]

        scr.fill(black)
        drawBorderLine(scr, white, area)
        pygame.draw.rect(scr, white, player1)
        pygame.draw.rect(scr, white, player2)
        pygame.draw.rect(
            scr, white, (0, scoreBoard.bottom, scoreBoard.width, -2))
        drawButton(pauseButton)
        scr.blit(p1Text, p1TextBox)
        scr.blit(p2Text, p2TextBox)
        # pygame.draw.circle(scr, color, [round(ball.get("position")[0]), round(ball.get("position")[1])],
        #                     round(ball.get("radius")))
        pygame.draw.rect(
            scr,
            color,
            (round(
                ball.get("position")[0] -
                ball.get("radius")),
                round(
                ball.get("position")[1] -
                ball.get("radius")),
                round(
                ball.get("radius")) *
                2,
                round(
                ball.get("radius")) *
                2))
        pygame.display.flip()
        dt = clock.tick(600)
        dt /= 1000


if __name__ == "__main__":
    global black, white, scr, clock, modeType, events
    events = []
    loadHighscores()
    pygame.init()
    modeType = ("MULTI", "EASY", "MEDIUM", "HARD")
    black = (0, 0, 0)
    white = (255, 255, 255)
    scr = pygame.display.set_mode((800, 600))
    pygame.key.set_repeat(1, 1)
    clock = pygame.time.Clock()
    showMenu()
